import React from 'react';

const Home = React.lazy(() => import('./views/Home'));
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Dashboard2 = React.lazy(() => import('./views_/Dashboard'));
const Sites = React.lazy(() => import('./views/Sites'));
const Site = React.lazy(() => import('./views/Site'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/home', exact: true, name: 'Home', component: Home },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/manage/sites/:id', name: 'Site', component: Site, site: true },
  { path: '/manage/sites', name: 'Sites', component: Sites, site: true },
  { path: '/dashboard2', name: 'Dashboard2', component: Dashboard2 },
];

export default routes;
