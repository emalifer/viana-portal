export const getLocationByID = (locationID) => {
  return (dispatch, getState) => {
    if(getState().firebase.data.locations) {
      const location = getState().firebase.data.locations[locationID];
      if(location) {
        return location;
      }
    }
    return {};
  };
}

export const getLocationsBySubscriptionID = (subscriptionID) => {
  return (dispatch, getState) => {
    const locations = getState().firebase.data.locations;
    let locationsFiltered = {};

    if(locations) {
      for (const [locationID, locationValue] of Object.entries(locations)) {
        if(locationValue.subscriptionID === subscriptionID) {
          locationsFiltered[locationID] = locationValue;
        }
      }
    }

    return locationsFiltered;
  };
}

export const getLocationsBySubscriptionIDAndBySiteID = (subscriptionID, siteID) => {
  return (dispatch, getState) => {
    const locations = getState().firebase.data.locations;
    let locationsFiltered = {};

    if(locations) {
      for (const [locationID, locationValue] of Object.entries(locations)) {
        if(locationValue.subscriptionID === subscriptionID && locationValue.siteID === siteID) {
          locationsFiltered[locationID] = locationValue;
        }
      }
    }

    return locationsFiltered;
  };
}