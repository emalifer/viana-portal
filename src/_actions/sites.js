export const getSiteByID = (siteID) => {
  return (dispatch, getState) => {
    if(getState().firebase.data.sites) {
      const site = getState().firebase.data.sites[siteID];
      if(site) {
        return site;
      }
    }
    return {};
  };
}

export const getSitesBySubscriptionID = (subscriptionID) => {
  return (dispatch, getState) => {
    const sites = getState().firebase.data.sites;
    let sitesFiltered = {};

    if(sites) {
      for (const [siteID, siteValue] of Object.entries(sites)) {
        if(siteValue.subscriptionID === subscriptionID) {
          sitesFiltered[siteID] = siteValue;
        }
      }
    }

    return sitesFiltered;
  };
}

export const sortSitesByLocation = () => {
  return (dispatch, getState) => {
    let sites = getState().firebase.data.sites;

    let tree = {};
    
    if(sites) {
      // sites.forEach((site) => {
  
      //   if(site.location.country) {
      //     const countryIndex = tree.findIndex(i => i.name === site.location.country);
      //     if(countryIndex != -1) {
      //       if(site.location.state) {
      //         const stateIndex = tree[countryIndex].states.findIndex(j => j.name === site.location.state);
      //         if(stateIndex != -1) {
      //           tree[countryIndex].states[stateIndex].sites.push(site);
      //         } else {
      //           tree[countryIndex].states.push({name: site.location.state, collapsed: false, sites: []});
      //         }
      //       }
      //     } else {
      //       tree.push({name: site.location.country, collapsed: false, states: []});
  
      //       const countryIndex = tree.findIndex(i => i.name === site.location.country);
      //       if(site.location.state) {
      //         const stateIndex = tree[countryIndex].states.findIndex(j => j.name === site.location.state);
      //         if(stateIndex != -1) {
      //           tree[countryIndex].states[stateIndex].sites.push(site);
      //         } else {
      //           tree[countryIndex].states.push({name: site.location.state, collapsed: false, sites: []});
  
      //           const stateIndex = tree[countryIndex].states.findIndex(j => j.name === site.location.state);
      //           tree[countryIndex].states[stateIndex].sites.push(site);
      //         }
      //       }
      //     }
      //   }
      // });
      for (const [siteID, siteValue] of Object.entries(sites)) {
        if(siteValue.location.country) {
          if(tree[siteValue.location.country]) {
            if(siteValue.location.state) {
              if(tree[siteValue.location.country].states[siteValue.location.state]) {
                tree[siteValue.location.country].states[siteValue.location.state].sites[siteID] = Object.assign({collapsed: true}, siteValue);
              } else {
                tree[siteValue.location.country].states[siteValue.location.state] = { collapsed: false, sites: {} };
                tree[siteValue.location.country].states[siteValue.location.state].sites[siteID] = Object.assign({collapsed: true}, siteValue);
              }
            }
          } else {
            tree[siteValue.location.country] = { collapsed: false, states: {} };
            if(siteValue.location.state) {
              if(tree[siteValue.location.country].states[siteValue.location.state]) {
                tree[siteValue.location.country].states[siteValue.location.state].sites[siteID] = Object.assign({collapsed: true}, siteValue);
              } else {
                tree[siteValue.location.country].states[siteValue.location.state] = { collapsed: false, sites: {} };
                tree[siteValue.location.country].states[siteValue.location.state].sites[siteID] = Object.assign({collapsed: true}, siteValue);
              }
            }
          }
        }
      }
    }

    return tree;
  };
}