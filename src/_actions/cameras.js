export const getCameraByID = (cameraID) => {
  return (dispatch, getState) => {
    if(getState().firebase.data.cameras) {
      const camera = getState().firebase.data.cameras[cameraID];
      if(camera) {
        return camera;
      }
    }
    return {};
  };
}

export const getCamerasBySubscriptionID = (subscriptionID) => {
  return (dispatch, getState) => {
    const cameras = getState().firebase.data.cameras;
    let camerasFiltered = {};

    if(cameras) {
      for (const [cameraID, cameraValue] of Object.entries(cameras)) {
        if(cameraValue.subscriptionID === subscriptionID) {
          camerasFiltered[cameraID] = cameraValue;
        }
      }
    }

    return camerasFiltered;
  };
}

export const getCamerasBySubscriptionIDAndBySiteID = (subscriptionID, siteID) => {
  return (dispatch, getState) => {
    const cameras = getState().firebase.data.cameras;
    let camerasFiltered = {};

    if(cameras) {
      for (const [cameraID, cameraValue] of Object.entries(cameras)) {
        if(cameraValue.subscriptionID === subscriptionID && cameraValue.siteID === siteID) {
          camerasFiltered[cameraID] = cameraValue;
        }
      }
    }

    return camerasFiltered;
  };
}

export const getCamerasBySubscriptionIDAndBySiteIDAndByLocationID = (subscriptionID, siteID, locationID) => {
  return (dispatch, getState) => {
    const cameras = getState().firebase.data.cameras;
    let camerasFiltered = {};

    if(cameras) {
      for (const [cameraID, cameraValue] of Object.entries(cameras)) {
        if(cameraValue.subscriptionID === subscriptionID && cameraValue.siteID === siteID && cameraValue.locationID === locationID) {
          camerasFiltered[cameraID] = cameraValue;
        }
      }
    }

    return camerasFiltered;
  };
}