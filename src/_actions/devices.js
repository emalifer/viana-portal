export const getDeviceByID = (deviceID) => {
  return (dispatch, getState) => {
    if(getState().firebase.data.devices) {
      const device = getState().firebase.data.devices[deviceID];
      if(device) {
        return device;
      }
    }
    return {};
  };
}

export const getDevicesBySubscriptionID = (subscriptionID) => {
  return (dispatch, getState) => {
    const devices = getState().firebase.data.devices;
    let devicesFiltered = {};

    if(devices) {
      for (const [deviceID, deviceValue] of Object.entries(devices)) {
        if(deviceValue.subscriptionID === subscriptionID) {
          devicesFiltered[deviceID] = deviceValue;
        }
      }
    }

    return devicesFiltered;
  };
}

export const getDevicesBySubscriptionIDAndBySiteID = (subscriptionID, siteID) => {
  return (dispatch, getState) => {
    const devices = getState().firebase.data.devices;
    let devicesFiltered = {};

    if(devices) {
      for (const [deviceID, deviceValue] of Object.entries(devices)) {
        if(deviceValue.subscriptionID === subscriptionID && deviceValue.siteID === siteID) {
          devicesFiltered[deviceID] = deviceValue;
        }
      }
    }

    return devicesFiltered;
  };
}

export const getDevicesBySubscriptionIDAndBySiteIDAndByLocationID = (subscriptionID, siteID, locationID) => {
  return (dispatch, getState) => {
    const devices = getState().firebase.data.devices;
    let devicesFiltered = {};

    if(devices) {
      for (const [deviceID, deviceValue] of Object.entries(devices)) {
        if(deviceValue.subscriptionID === subscriptionID && deviceValue.siteID === siteID && deviceValue.locationID === locationID) {
          devicesFiltered[deviceID] = deviceValue;
        }
      }
    }

    return devicesFiltered;
  };
}

export const sortDevicesBySiteID = (siteID) => {
  return (dispatch, getState) => {
    let sites = getState().firebase.data.sites;
    let locations = getState().firebase.data.locations;
    let devices = getState().firebase.data.devices;
    let cameras = getState().firebase.data.cameras;
    let screens = getState().firebase.data.screens;

    let tree = {};
    
    if(sites && locations) {
      // let site = sites.find((site) => {
      //   return site.id === siteID;
      // });

      let site = sites[siteID];

      if(site) {
        site = Object.assign({}, site);
        site.locations = {};
        site.collapsed = false;
        tree[siteID] = site;

        // locations.forEach((location) => {

        //   if(location.siteID === siteID) {
        //     location.devices = [];
        //     location.collapsed = false;

        //     if(devices) {
        //       devices.forEach((device) => {
        //         device.type = 'device';
        //         if(device.siteID === siteID && device.locationID === location.id) {
        //           location.devices.push(device);
        //         }
        //       })
        //     }  

        //     if(cameras) {
        //       cameras.forEach((camera) => {
        //         camera.type = 'camera';
        //         if(camera.siteID === siteID && camera.locationID === location.id) {
        //           location.devices.push(camera);
        //         }
        //       })
        //     }
            
        //     if(screens) {
        //       screens.forEach((screen) => {
        //         screen.type = 'screen';
        //         if(screen.siteID === siteID && screen.locationID === location.id) {
        //           location.devices.push(screen);
        //         }
        //       })
        //     }

        //     tree[0].locations.push(location);

        //   }

        // });

        for (const [locationID, locationValue] of Object.entries(locations)) {

          if(locationValue.siteID === siteID) {
            locationValue.devices = {};
            locationValue.collapsed = false;

            if(devices) {
              // devices.forEach((device) => {
              //   device.type = 'device';
              //   if(device.siteID === siteID && device.locationID === location.id) {
              //     location.devices.push(device);
              //   }
              // })
              for (const [deviceID, deviceValue] of Object.entries(devices)) {
                deviceValue.type = 'device';
                if(deviceValue.siteID === siteID && deviceValue.locationID === locationID) {
                  locationValue.devices[deviceID] = deviceValue;
                }
              }
            }  

            if(cameras) {
              // cameras.forEach((camera) => {
              //   camera.type = 'camera';
              //   if(camera.siteID === siteID && camera.locationID === location.id) {
              //     location.devices.push(camera);
              //   }
              // })
              for (const [cameraID, cameraValue] of Object.entries(cameras)) {
                cameraValue.type = 'camera';
                if(cameraValue.siteID === siteID && cameraValue.locationID === locationID) {
                  locationValue.devices[cameraID] = cameraValue;
                }
              }
            }
            
            if(screens) {
              // screens.forEach((screen) => {
              //   screen.type = 'screen';
              //   if(screen.siteID === siteID && screen.locationID === location.id) {
              //     location.devices.push(screen);
              //   }
              // })
              for (const [screenID, screenValue] of Object.entries(screens)) {
                screenValue.type = 'screen';
                if(screenValue.siteID === siteID && screenValue.locationID === locationID) {
                  locationValue.devices[screenID] = screenValue;
                }
              }
            }

            tree[siteID].locations[locationID] = locationValue;

          }

        };

      }
    }

    return tree;
  };
}