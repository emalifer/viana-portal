export const getScreensBySubscriptionID = (subscriptionID) => {
  return (dispatch, getState) => {
    const screens = getState().firebase.data.screens;
    let screensFiltered = {};

    if(screens) {
      for (const [screenID, screenValue] of Object.entries(screens)) {
        if(screenValue.subscriptionID === subscriptionID) {
          screensFiltered[screenID] = screenValue;
        }
      }
    }

    return screensFiltered;
  };
}

export const getScreensBySubscriptionIDAndBySiteID = (subscriptionID, siteID) => {
  return (dispatch, getState) => {
    const screens = getState().firebase.data.screens;
    let screensFiltered = {};

    if(screens) {
      for (const [screenID, screenValue] of Object.entries(screens)) {
        if(screenValue.subscriptionID === subscriptionID && screenValue.siteID === siteID) {
          screensFiltered[screenID] = screenValue;
        }
      }
    }

    return screensFiltered;
  };
}

export const getScreensBySubscriptionIDAndBySiteIDAndByLocationID = (subscriptionID, siteID, locationID) => {
  return (dispatch, getState) => {
    const screens = getState().firebase.data.screens;
    let screensFiltered = {};

    if(screens) {
      for (const [screenID, screenValue] of Object.entries(screens)) {
        if(screenValue.subscriptionID === subscriptionID && screenValue.siteID === siteID && screenValue.locationID === locationID) {
          screensFiltered[screenID] = screenValue;
        }
      }
    }

    return screensFiltered;
  };
}