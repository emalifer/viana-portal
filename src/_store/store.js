import { createBrowserHistory } from 'history';
import { applyMiddleware, compose, createStore } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import { getFirebase } from 'react-redux-firebase';
import createRootReducer from '../_reducers';

export const history = createBrowserHistory();

const initialState = {};
const enhancers = [];
const middleware = [
  // thunk,
  thunk.withExtraArgument(getFirebase), 
  routerMiddleware(history)
];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

export default function configureStore() {
  const store = createStore(
    createRootReducer(history), // root reducer with router state
    initialState,
    compose(
      applyMiddleware(
        ...middleware
      ),
      ...enhancers
    ),
  )

  return store
}