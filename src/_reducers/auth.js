const initialState = {
  subscriptionID: 'SCvMPp8zg2HQhMq2iOvv',
  subscriptionName: 'Chemist Warehouse',
  subscriptionImage: 'chemistwarehouse.png',
  id: '484f3bef-6bec-4e13-89c6-a91b0ce1e2f5',
  profile: {
    firstName: 'Christine',
    lastName: 'Vega'
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
}