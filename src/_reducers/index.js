import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { ReactReduxFirebaseProvider, firebaseReducer } from 'react-redux-firebase';
import auth from './auth';

const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  auth,
  firebase: firebaseReducer
  // window,
});

export default createRootReducer
