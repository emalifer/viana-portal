export default {
  items: [
    {
      name: 'Home',
      url: '/home',
      icon: 'icon-home',
    },
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      divider: true,
      class: 'border-top ml-3 mr-3'
    },
    {
      title: true,
      name: 'Manage',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Sites',
      url: '/manage/sites',
      icon: 'cil-location-pin',
    },
    {
      name: 'Devices',
      url: '/manage/devices',
      icon: 'cil-laptop',
      attributes: {
        class: 'disabled'
      }
    },
    {
      name: 'Cameras',
      url: '/manage/cameras',
      icon: 'cil-camera',
      attributes: {
        class: 'disabled'
      }
    },
    {
      name: 'Screens',
      url: '/manage/screens',
      icon: 'cil-devices',
      attributes: {
        class: 'disabled'
      }
    },
    {
      divider: true,
      class: 'border-top ml-3 mr-3'
    },
    {
      title: true,
      name: 'Downloads',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Insights',
      url: '/downloads/insights',
      icon: 'icon-drop',
      attributes: {
        class: 'disabled'
      }
    },
    {
      name: 'Installers',
      url: '/downloads/installers',
      icon: 'icon-drop',
      attributes: {
        class: 'disabled'
      }
    },
    {
      divider: true,
      class: 'border-top ml-3 mr-3'
    },
    {
      title: true,
      name: 'Customize',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Profile',
      url: '/profile',
      icon: 'cil-user',
      attributes: {
        class: 'disabled'
      }
    },
    {
      name: 'Settings',
      url: '/settings',
      icon: 'cil-settings',
      attributes: {
        class: 'disabled'
      }
    }
  ]
};

export const footerNav = {
  items: [
    {
      name: 'Profile',
      url: '/profile',
      icon: 'cil-user',
    },
    {
      name: 'Settings',
      url: '/settings',
      icon: 'cil-settings',
    }
  ]
}