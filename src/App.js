import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import configureStore, { history } from './_store/store';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// import { renderRoutes } from 'react-router-config';

import firebase from 'firebase/app';
import  'firebase/database';
import 'firebase/auth';
import { ReactReduxFirebaseProvider, firebaseReducer } from 'react-redux-firebase';

import './App.scss';

const fbConfig = {
  apiKey: "AIzaSyBEFR4482KW3nhrhiwLMEWmuAyYtidiAf8",
  authDomain: "viana-ui-test.firebaseapp.com",
  databaseURL: "https://viana-ui-test.firebaseio.com",
  projectId: "viana-ui-test",
  storageBucket: "viana-ui-test.appspot.com",
  messagingSenderId: "87103270935",
  appId: "1:87103270935:web:0de70329238eb77c082c52",
  measurementId: "G-2TH4W2KH5X"
};

firebase.initializeApp(fbConfig);
firebase.auth().onAuthStateChanged(async user => {
  if (!user) {
    await firebase.auth().signInAnonymously();
  }
});

const store = configureStore();

const rrfConfig = {
}

const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch
}

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

// Pages
const Login = React.lazy(() => import('./views/Pages/Login'));
const Register = React.lazy(() => import('./views/Pages/Register'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));
const Page500 = React.lazy(() => import('./views/Pages/Page500'));

function App(){
  return (
    <Provider store={store}>
      <ReactReduxFirebaseProvider {...rrfProps}>
        <ConnectedRouter history={history}>
            <React.Suspense fallback={loading()}>
              <Switch>
                <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
                <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
                <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
                <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
                <Route path="/" name="Home" render={props => <DefaultLayout {...props}/>} />
              </Switch>
            </React.Suspense>
        </ConnectedRouter>
      </ReactReduxFirebaseProvider>
    </Provider>
  );
}

export default App;
