import React, { Component, lazy, Suspense } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Doughnut, Bar } from 'react-chartjs-2';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import numeral from 'numeraljs';
import PropTypes from 'prop-types';

const doughnutOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: true,
    position: 'right'
  }
};

class Widget extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  render () {

    let graph;
    switch(this.props.data.type) {
      case 'Bar':
        graph = <Bar data={this.props.data} options={doughnutOpts} height={200} />;
        break;
      case 'Doughnut':
        graph = <Doughnut data={this.props.data} options={doughnutOpts} height={200} />;
        break;
      default:
        break;
    }

    return (
      <Col xs={this.props.data.size.xs} sm={this.props.data.size.sm} md={this.props.data.size.md} lg={this.props.data.size.lg} xl={this.props.data.size.xl}>
        <Card className="pb-4">
          <CardBody className="pb-0">
            <ButtonGroup className="float-right">
              <ButtonDropdown id='card1' isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                <DropdownToggle caret className="p-0" color="">
                  <i className="icon-settings"></i>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>Hide</DropdownItem>
                  <DropdownItem>Configure</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </ButtonGroup>
            <div className="text-value">{this.props.data.title}</div>
            {this.props.data.value &&
              <div className="mb-2">{numeral(this.props.data.value).format(this.props.data.format)} {this.props.data.unit}</div>
            }
            {!this.props.data.value &&
              <div className="mb-2">&nbsp;</div>
            }
          </CardBody>
          <div className="chart-wrapper mx-3" style={{ height: '250px' }}>
            {graph}
          </div>
        </Card>
      </Col>
    )
  }
}

Widget.propTypes = {
  data: PropTypes.shape({
    size: PropTypes.shape({
      xs: PropTypes.number.isRequired
    })
  })
}

Widget.defaultProps = {
  data: {
    size: {
      xs: 12,
      sm: 6,
      md: 4,
      lg: 4,
      xl: 4
    },
  },
  type: 'Bar'
}


export default Widget;