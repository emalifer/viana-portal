import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import {Helmet} from "react-helmet";

import Widget from './Widget/Widget';

const otsData = {
  title: 'OTS',
  value: 17291,
  unit: 'Persons',
  format: '0,00',
  size: {
    xs: 12,
    sm: 6,
    md: 6,
    lg: 6,
    xl: 6
  },
  type: 'Doughnut',
  labels: ['Site A', 'Site B'],
  datasets: [
    {
      label: 'OTS',
      backgroundColor: ['#CCEBED', '#0099A7'],
      borderColor: 'rgba(255,255,255,.55)',
      data: [3, 18],
    },
  ],
};

const watchersData = {
  title: 'Watchers',
  value: 17291,
  unit: 'Persons',
  format: '0,00',
  size: {
    xs: 12,
    sm: 6,
    md: 6,
    lg: 6,
    xl: 6
  },
  type: 'Doughnut',
  labels: ['Site A', 'Site B'],
  datasets: [
    {
      label: 'OTS',
      backgroundColor: ['#E3D0FF', '#9C6ADE'],
      borderColor: 'rgba(255,255,255,.55)',
      data: [3, 18],
    },
  ],
};

const dwelltimeData = {
  title: 'Dwell Time',
  value: 20.40,
  unit: 'Hours',
  format: '0,00.00',
  size: {
    xs: 12,
    sm: 6,
    md: 6,
    lg: 6,
    xl: 6
  },
  type: 'Doughnut',
  labels: ['Site A', 'Site B'],
  datasets: [
    {
      label: 'OTS',
      backgroundColor: ['#ECABD2', '#EE0095'],
      borderColor: 'rgba(255,255,255,.55)',
      data: [3, 18],
    },
  ],
};

const conversionratioData = {
  title: 'Conversion Ratio',
  value: 11,
  unit: '%',
  format: '0',
  size: {
    xs: 12,
    sm: 6,
    md: 6,
    lg: 6,
    xl: 6
  },
  type: 'Doughnut',
  labels: ['Conversion Ratio'],
  datasets: [
    {
      label: 'Conversion Ratio',
      backgroundColor: ['#1565C0'],
      borderColor: 'rgba(255,255,255,.55)',
      data: [11, 89],
    },
  ],
};

const otsvswatchersperlocationData = {
  title: 'OTS vs Watchers per location',
  size: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },
  type: 'Bar',
  labels: ['Site A', 'Site B'],
  datasets: [
    {
      label: 'OTS',
      backgroundColor: '#EEC200',
      data: [3,5],
    },
    {
      label: 'Watchers',
      backgroundColor: '#F6E7A4',
      data: [8,15],
    }
  ],
};

const dwellvsattentiontimeperlocationData = {
  title: 'Dwell vs Attention Time per location',
  size: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },
  type: 'Bar',
  labels: ['Site A', 'Site B'],
  datasets: [
    {
      label: 'Dwell Time',
      backgroundColor: '#EE8700',
      data: [3,5],
    },
    {
      label: 'Attention Time',
      backgroundColor: '#F6C8A1',
      data: [8,15],
    }
  ],
};

const otswatchersconversionratioData = {
  title: 'OTS, Watchers & Conversion Ratio',
  size: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },
  type: 'Bar',
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  datasets: [
    
    {
      label: 'Watchers',
      fill: false,
      borderColor: '#496685',
      backgroundColor: '#496685',
      data: [5,12,7,10,11,7,5,9,14,16,20,24],
      type: 'line'
    },
    {
      label: 'OTS',
      backgroundColor: '#A6CBC4',
      data: [3,5,6,4,5,3,4,5,6,7,8,10]
    },
    {
      label: 'Conversion Ratio',
      backgroundColor: '#539D91',
      data: [8,15,10,12,15,11,8,14,18,20,24,29],
    }
  ]
};


class Dashboard extends Component {
  constructor(props) {
    super(props);
  }

  render() {

    const { subscriptionID, subscriptionName, profile, subscriptionImage } = this.props.auth;

    return (
      <div className="animated fadeIn" style={{width: '100%'}}>

        <Helmet>
          <title>Viana: {subscriptionName} - Home</title>
        </Helmet>

        <h3 className="page-title mb-4"><img src={`/assets/img/${subscriptionImage}`} width="30" className="mr-3" /><span className="d-inline-block" style={{verticalAlign: 'middle'}}><NavLink to={'/home'}>{subscriptionName}</NavLink> / Dashboard</span>
        
          <Form className="float-right form-inline">
            <FormGroup className="float-right">
              <Input type="text" name="site" id="site" placeholder="Search Dashboards" className="mr-2 form-control-lg shadow-sm" disabled />
              <a className="btn btn-primary btn-lg text-white disabled"><i className="fa fa-plus mr-2"></i>New Dashboard</a>
            </FormGroup>
          </Form>
        </h3>

        <Row className="">
          <Col>
            <Row>
              <Widget data={otsData} />
              <Widget data={watchersData} />
              <Widget data={dwelltimeData} />
              <Widget data={conversionratioData} />
            </Row>
          </Col>
          <Col>
            <Row>
              <Widget data={otsvswatchersperlocationData} />
              <Widget data={dwellvsattentiontimeperlocationData} />
            </Row>
          </Col>
        </Row>
        <Row>
          <Col>
            <Row>
              <Widget data={otswatchersconversionratioData} />
            </Row>
          </Col>
        </Row>

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard);
