import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";
import TreeView from 'react-treeview';

import * as SitesActions from '../../../_actions/sites';
import * as LocationsActions from '../../../_actions/locations';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';

class SiteDeviceTree extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sortedDevicesByLocation: {}
    }
  }

  componentDidMount = () => {
    this.sortDevicesByListing();
  }

  componentDidUpdate = (prevProps) => {
    if(prevProps.sites !== this.props.sites) {
      this.sortDevicesByListing();
    }
    if(prevProps.sites !== this.props.sites) {
      this.sortDevicesByListing();
    }
    if(prevProps.locations !== this.props.locations) {
      this.sortDevicesByListing();
    }
    if(prevProps.devices !== this.props.devices) {
      this.sortDevicesByListing();
    }
    if(prevProps.cameras !== this.props.cameras) {
      this.sortDevicesByListing();
    }
    if(prevProps.screens !== this.props.screens) {
      this.sortDevicesByListing();
    }
  }

  sortDevicesByListing = () => {
    const { id } = this.props.match.params;
    this.setState({sortedDevicesByLocation: this.props.sortDevicesBySiteID(id)});
  }

  toggle = (i = null, j = null) => {
    if(j != null) {
      this.setState(
        produce(this.state, draftState => {
          draftState.sortedDevicesByLocation[i].locations[j].collapsed = !this.state.sortedDevicesByLocation[i].locations[j].collapsed;
        })
      );
    } else {
      if(i != null) {
        this.setState(
          produce(this.state, draftState => {
            draftState.sortedDevicesByLocation[i].collapsed = !this.state.sortedDevicesByLocation[i].collapsed;
          })
        );
      }
    }
  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;
    const { sortedDevicesByLocation } = this.state;

    const site = this.props.getSiteByID(id);
    const locations = this.props.getLocationsBySubscriptionIDAndBySiteID(subscriptionID, id);
    const devices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);
    const cameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);
    const screens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <div className="animated fadeIn ">
        <div className="card callout callout-info mt-0 pb-3">
          <h5 className="pt-3 mb-3">Device Tree</h5>
          
          {/* {sortedDevicesByLocation.map((site, i) => {

            const siteLabel = <a className={`btn mb-1 ${site.collapsed ? `btn-link` : `btn-link`}`} onClick={() => { this.toggle(i) }}>{site.name}</a>;

            return (
              <TreeView key={`site-${i}`} nodeLabel={siteLabel} defaultCollapsed={site.collapsed} collapsed={site.collapsed}>
                {site.locations.map((location, j) => {

                  const locationLabel = <a className={`btn mb-1 ml-2 ${location.collapsed ? `btn-link` : `btn-link`}`} onClick={() => { this.toggle(i,j) }}>{location.name}</a>;

                  return (
                    <TreeView key={`location-${j}`} nodeLabel={locationLabel} defaultCollapsed={location.collapsed} collapsed={location.collapsed}>
                      {location.devices.map((device, k) => {
                        return (
                          <div className="btn-group mb-1 ml-4 d-block" key={`device-${k}`}>
                              {(() => {
                                switch(device.type) {
                                  case 'device':
                                    return (
                                      <NavLink to={`/manage/sites/${site.id}/devices/${device.id}`} className={`btn ${site.collapsed ? `btn-link` : `btn-link`}`} title="Go to device">
                                        <i className="cil-laptop mr-3" /> {device.name}
                                      </NavLink>
                                    );
                                  case 'camera':
                                    return (
                                      <NavLink to={`/manage/sites/${site.id}/cameras/${device.id}`} className={`btn ${site.collapsed ? `btn-link` : `btn-link`}`} title="Go to camera">
                                        <i className="cil-camera mr-3" /> {device.name}
                                      </NavLink>
                                    );
                                  case 'camera':
                                    return (
                                      <NavLink to={`/manage/sites/${site.id}/screens/${device.id}`} className={`btn ${site.collapsed ? `btn-link` : `btn-link`}`} title="Go to screens">
                                        <i className="cil-devices mr-3" /> {device.name}
                                      </NavLink>
                                    );
                                  default:
                                    break;
                                }
                              })()}
                          </div>
                        );
                      })}
                    </TreeView>
                  );
                })}
              </TreeView>
            );
          })} */}

          

          {Object.entries(sortedDevicesByLocation).map((site) => {
            const siteLabel = <a className={`btn mb-1 ${site[1].collapsed ? `btn-link` : `btn-link`}`} onClick={() => { this.toggle(site[0]) }}>{site[1].name}</a>;

            return (
              <TreeView key={`site-${site[0]}`} nodeLabel={siteLabel} defaultCollapsed={site[1].collapsed} collapsed={site[1].collapsed}>
                {Object.entries(site[1].locations).map((location) => {
                  const locationLabel = <a className={`btn mb-1 ${location[1].collapsed ? `btn-link` : `btn-link`}`} onClick={() => { this.toggle(site[0], location[0]) }}>{location[1].name}</a>;

                  return (
                    <TreeView key={`location-${location[0]}`} nodeLabel={locationLabel} defaultCollapsed={location[1].collapsed} collapsed={location[1].collapsed}>
                      {Object.entries(location[1].devices).map((device) => {
                        return (
                          <div className="btn-group mb-1 ml-4 d-block" key={`device-${device[0]}`}>
                              {(() => {
                                switch(device[1].type) {
                                  case 'device':
                                    return (
                                      <NavLink to={`/manage/sites/${site[0]}/devices/${device[0]}`} className={`btn ${site[1].collapsed ? `btn-link` : `btn-link`}`} title="Go to device">
                                        <i className="cil-laptop mr-3" /> {device[1].name}
                                      </NavLink>
                                    );
                                  case 'camera':
                                    return (
                                      <NavLink to={`/manage/sites/${site[0]}/cameras/${device[0]}`} className={`btn ${site[1].collapsed ? `btn-link` : `btn-link`}`} title="Go to camera">
                                        <i className="cil-camera mr-3" /> {device[1].name}
                                      </NavLink>
                                    );
                                  case 'camera':
                                    return (
                                      <NavLink to={`/manage/sites/${site[0]}/screens/${device[0]}`} className={`btn ${site[1].collapsed ? `btn-link` : `btn-link`}`} title="Go to screens">
                                        <i className="cil-devices mr-3" /> {device[1].name}
                                      </NavLink>
                                    );
                                  default:
                                    break;
                                }
                              })()}
                          </div>
                        );
                      })}
                    </TreeView>
                  )
                })}
              </TreeView>
            )
          })}

        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    sites: state.firebase.data.sites,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getLocationsBySubscriptionIDAndBySiteID: LocationsActions.getLocationsBySubscriptionIDAndBySiteID,
    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID,

    sortDevicesBySiteID: DevicesActions.sortDevicesBySiteID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiteDeviceTree);
