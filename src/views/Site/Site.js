import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {Helmet} from "react-helmet";

import * as SitesActions from '../../_actions/sites';
import * as DevicesActions from '../../_actions/devices';
import * as CamerasActions from '../../_actions/cameras';
import * as ScreensActions from '../../_actions/screens';
import SiteNewCamera from './SiteNewCamera/SiteNewCamera';
import SiteNewLocation from './SiteNewLocation/SiteNewLocation';
import SiteNewDevice from './SiteNewDevice/SiteNewDevice';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Pages
const SiteInformation = React.lazy(() => import('./SiteInformation/SiteInformation'));
const SiteLocations = React.lazy(() => import('./SiteLocations/SiteLocations'));
const SiteLocation = React.lazy(() => import('./SiteLocation/SiteLocation'));
const SiteDevices = React.lazy(() => import('./SiteDevices/SiteDevices'));
const SiteDevice = React.lazy(() => import('./SiteDevice/SiteDevice'));
const SiteCameras = React.lazy(() => import('./SiteCameras/SiteCameras'));
const SiteCamera = React.lazy(() => import('./SiteCamera/SiteCamera'));
const SiteScreens = React.lazy(() => import('./SiteScreens/SiteScreens'));
const SiteConfigurations = React.lazy(() => import('./SiteConfigurations/SiteConfigurations'));
const SiteLogs = React.lazy(() => import('./SiteLogs/SiteLogs'));

class Site extends Component {
  constructor(props) {
    super(props);

    this.state = {
      siteNewDeviceModal: false,
      siteNewCameraModal: false,
      siteNewLocationModal: false
    };
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, subscriptionImage, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    const devices = this.props.getDevicesBySubscriptionID(subscriptionID);
    const cameras = this.props.getCamerasBySubscriptionID(subscriptionID);
    const screens = this.props.getScreensBySubscriptionID(subscriptionID);

    return (
      <div className="animated fadeIn d-flex flex-column" style={{width: '100%'}}>

        <Helmet>
          <title>{`Viana: ${subscriptionName} - Sites - ${site.name}`}</title>
        </Helmet>

        <h3 className="mb-2 page-title"><img src={`/assets/img/${subscriptionImage}`} width="30" className="mr-3" /><span className="d-inline-block" style={{verticalAlign: 'middle'}}><NavLink to={`/home`}>{subscriptionName}</NavLink> / <NavLink to={`/manage/sites`}>Sites</NavLink> / {site.name}</span>

        <Form className="float-right form-inline">
          <ButtonDropdown isOpen={this.state.add} toggle={() => { this.setState({ add: !this.state.add }); }}>
            <DropdownToggle caret color="primary" size="lg">
              <span className="mr-2">New</span>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem onClick={() => this.setState({siteNewLocationModal: !this.state.siteNewLocationModal})}>New Location</DropdownItem>
              <DropdownItem onClick={() => this.setState({siteNewDeviceModal: !this.state.siteNewDeviceModal})}><i className="mr-1 cil-laptop" />New Device</DropdownItem>
              <DropdownItem onClick={() => this.setState({siteNewCameraModal: !this.state.siteNewCameraModal})}><i className="mr-1 cil-camera" />New Camera</DropdownItem>
              <DropdownItem disabled><i className="mr-1 cil-devices" />New Screen</DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>
        </Form>

        </h3>

        <div>
          <ul className="site-nav nav">
            <li className="nav-item">
              <NavLink className="nav-link  border-bottom" exact to={`/manage/sites/${id}`} activeClassName="active">Site Info</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link border-bottom" to={`/manage/sites/${id}/locations`} activeClassName="active">Site Locations</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link border-bottom" to={`/manage/sites/${id}/devices`} activeClassName="active">Site Devices</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link border-bottom" to={`/manage/sites/${id}/cameras`} activeClassName="active">Site Cameras</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link border-bottom disabled" to={`/manage/sites/${id}/screens`} activeClassName="active">Site Screens</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link border-bottom disabled" to={`/manage/sites/${id}/configurations`} activeClassName="active">Site Configurations</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link border-bottom disabled" to={`/manage/sites/${id}/logs`} activeClassName="active">Site Logs</NavLink>
            </li>
          </ul>
        </div>

        <div className="bg-white shadow" style={{margin: '0 -30px', minHeight: 'calc(100vh - 172px)'}}>
          <Container fluid className="mt-4">
            <Row>
              <Col>
                <React.Suspense fallback={loading()}>
                  <Switch>
                    <Route exact path="/manage/sites/:id" name="Site Information Page" render={props => <SiteInformation {...props} />} />
                    <Route exact path="/manage/sites/:id/locations" name="Site Locations Page" render={props => <SiteLocations {...props} />} />
                    <Route exact path="/manage/sites/:id/locations/:locationID" name="Site Location Page" render={props => <SiteLocation {...props} />} />
                    <Route exact path="/manage/sites/:id/devices" name="Site Devices Page" render={props => <SiteDevices {...props} />} />
                    <Route exact path="/manage/sites/:id/devices/:deviceID" name="Site Device Page" render={props => <SiteDevice {...props} />} />
                    <Route exact path="/manage/sites/:id/cameras" name="Site Cameras Page" render={props => <SiteCameras {...props} />} />
                    <Route exact path="/manage/sites/:id/cameras/:cameraID" name="Site Camera Page" render={props => <SiteCamera {...props} />} />
                    <Route exact path="/manage/sites/:id/cameras/:cameraID/live" name="Site Camera Page" render={props => <SiteCamera {...props} />} />
                    <Route exact path="/manage/sites/:id/screens" name="Site Screens Page" render={props => <SiteScreens {...props} />} />
                    <Route exact path="/manage/sites/:id/configurations" name="Site Configurations Page" render={props => <SiteConfigurations {...props} />} />
                    <Route exact path="/manage/sites/:id/logs" name="Site Logs Page" render={props => <SiteLogs {...props} />} />
                  </Switch>
                </React.Suspense>
              </Col>
            </Row>
          </Container>
        </div>

        <SiteNewDevice isOpen={this.state.siteNewDeviceModal} toggle={() => { this.setState({siteNewDeviceModal: !this.state.siteNewDeviceModal}) }} className={'m'} {...this.props} />
        <SiteNewCamera isOpen={this.state.siteNewCameraModal} toggle={() => { this.setState({siteNewCameraModal: !this.state.siteNewCameraModal}) }} className={'m'} {...this.props} />
        <SiteNewLocation isOpen={this.state.siteNewLocationModal} toggle={() => { this.setState({siteNewLocationModal: !this.state.siteNewLocationModal}) }} className={'m'} {...this.props} />

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    sites: state.firebase.data.sites
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionID: DevicesActions.getDevicesBySubscriptionID,
    getCamerasBySubscriptionID: CamerasActions.getCamerasBySubscriptionID,
    getScreensBySubscriptionID: ScreensActions.getScreensBySubscriptionID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Site);
