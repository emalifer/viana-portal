import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";

import * as SitesActions from '../../../_actions/sites';
import * as LocationsActions from '../../../_actions/locations';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';

class SiteLocations extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    const devices = this.props.getDevicesBySubscriptionID(subscriptionID);
    const cameras = this.props.getCamerasBySubscriptionID(subscriptionID);
    const screens = this.props.getScreensBySubscriptionID(subscriptionID);

    const siteDevices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <div className="animated fadeIn d-flex flex-column" style={{width: '100%'}}>

        <Helmet>
          <title>{`Viana: ${subscriptionName} - Sites - ${site.name}`}</title>
        </Helmet>

        <Row className="mb-4">
          <Col>
            <h4 className="">All Devices <span className="badge badge-pill badge-primary ml-2">{Object.entries(siteDevices).length}</span></h4>
          </Col>
          <Col>
            <Form className="float-right form-inline">
              <FormGroup className="float-right">
                <Input type="text" name="devices" id="devices" placeholder="Search Devices" className="mr-2 form-control" disabled />
                <a className="btn btn-outline-primary text-primary disabled"><i className="fa fa-plus mr-2"></i>Filter Results</a>
              </FormGroup>
            </Form>
          </Col>
        </Row>

        <table className="table">
          <thead>
            <tr>
              <th scope="col">Device Name</th>
              <th scope="col">Location</th>
              <th scope="col">Camera Count</th>
              <th scope="col">Screen Count</th>
              <th scope="col">Status</th>
              <th scope="col">App Version</th>
              <th scope="col" width="150"></th>
            </tr>
          </thead>
          {Object.entries(siteDevices).length > 0 &&
            <tbody>
              {Object.entries(siteDevices).map(device => {

                const deviceLocation = this.props.getLocationByID(device[1].locationID);
                // const cameraDevice= this.props.getDeviceByID(camera[1].deviceID);

                return (
                  <tr key={`location-${device[0]}`}>
                    <td scope="col"><NavLink className="" exact to={`/manage/sites/${id}/devices/${device[0]}`}>{device[1].name}</NavLink></td>
                    <td scope="col"><NavLink className="" exact to={`/manage/sites/${id}/locations/${device[1].locationID}`}>{deviceLocation.name}</NavLink></td>
                    <td scope="col"></td>
                    <td scope="col"></td>
                    <td scope="col"><span className="badge badge-pill badge-secondary mr-3">&nbsp;&nbsp;</span><span className="text-muted">NA</span></td>
                    <td scope="col"><span className="text-muted">NA</span></td>
                    <td scope="col">
                      <NavLink className="mr-3" exact to={`/manage/sites/${id}/devices/${device[0]}`}>View Profile <i className="ml-2 fas fa-arrow-circle-right" /></NavLink>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          }
        </table>

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,
    getDeviceByID: DevicesActions.getDeviceByID,
    getLocationByID: LocationsActions.getLocationByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,
    getDevicesBySubscriptionID: DevicesActions.getDevicesBySubscriptionID,
    
    getDevicesBySubscriptionID: DevicesActions.getDevicesBySubscriptionID,
    getCamerasBySubscriptionID: CamerasActions.getCamerasBySubscriptionID,
    getScreensBySubscriptionID: ScreensActions.getScreensBySubscriptionID,

    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiteLocations);
