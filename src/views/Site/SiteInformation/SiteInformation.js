import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";

import * as SitesActions from '../../../_actions/sites';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';

const SiteMap = React.lazy(() => import( '../SiteMap/SiteMap'));
const SiteActivity = React.lazy(() => import( '../SiteActivity/SiteActivity'));
const SiteDeviceTree = React.lazy(() => import( '../SiteDeviceTree/SiteDeviceTree'));

const SiteProfile = React.lazy(() => import( '../SiteProfile/SiteProfile'));

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

class SiteInformation extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    const devices = this.props.getDevicesBySubscriptionID(subscriptionID);
    const cameras = this.props.getCamerasBySubscriptionID(subscriptionID);
    const screens = this.props.getScreensBySubscriptionID(subscriptionID);

    return (
      <div className="animated fadeIn d-flex flex-column" style={{width: '100%'}}>

        <Helmet>
          <title>{`Viana: ${subscriptionName} - Sites - ${site.name}`}</title>
        </Helmet>

        <Row>
          <Col>
            <React.Suspense fallback={loading()}>
              <SiteMap {...this.props} />
            </React.Suspense>

            <React.Suspense fallback={loading()}>
              <SiteActivity {...this.props} />
            </React.Suspense>
          </Col>
          
          <Col xs={12} sm={6} md={6} lg ={5} xl={4}>
            <React.Suspense fallback={loading()}>
              <SiteProfile {...this.props} />
            </React.Suspense>
            <React.Suspense fallback={loading()}>
              <SiteDeviceTree {...this.props} />
            </React.Suspense>
          </Col>
        </Row>

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionID: DevicesActions.getDevicesBySubscriptionID,
    getCamerasBySubscriptionID: CamerasActions.getCamerasBySubscriptionID,
    getScreensBySubscriptionID: ScreensActions.getScreensBySubscriptionID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiteInformation);
