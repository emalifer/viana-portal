import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Label, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";
import TreeView from 'react-treeview';
import { firebaseConnect } from 'react-redux-firebase';

import * as SitesActions from '../../../_actions/sites';
import * as LocationsActions from '../../../_actions/locations';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';

class SiteNewLocation extends Component {
  constructor(props) {
    super(props);
  }

  addLocation = (event) => {
    event.preventDefault();

    console.log('add location');
    const { subscriptionID } = this.props.auth;

    this.props.firebase.push('/locations', 
      {
        name: event.target.name.value,
        description: event.target.description.value,
        siteID: event.target.siteID.value,
        subscriptionID 
      });
    this.props.toggle();
  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;
    const { isOpen, toggle, className } = this.props; 

    const site = this.props.getSiteByID(id);
    const sites = this.props.getSitesBySubscriptionID(subscriptionID, id);
    const locations = this.props.getLocationsBySubscriptionIDAndBySiteID(subscriptionID, id);
    const devices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);
    const cameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);
    const screens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <Modal isOpen={isOpen} toggle={toggle} size={'md'} centered className={className}>
        <Form onSubmit={(e) => { this.addLocation(e); }}>
          <ModalHeader toggle={toggle}>Add New Location</ModalHeader>
          <ModalBody>
            <Row>
              <Col>
                <FormGroup>
                  <Label htmlFor="name">Location Name <span className="text-danger">*</span></Label>
                  <Input type="text" id="name" name="name" placeholder="Location Name" required />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label htmlFor="siteID">Assigned Site <span className="text-danger">*</span></Label>
                  <select id="siteID" name="siteID" className="form-control">
                    {Object.entries(sites).map((site) => {
                      return (
                        <option key={site[0]} value={site[0]}>{site[1].name}</option>
                      )
                    })}
                  </select>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                    <Label htmlFor="description">Location Description</Label>
                    <textarea id="description" name="description" rows="5" className="form-control">
                    </textarea>
                </FormGroup>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button color="light" onClick={toggle}>Cancel</Button>
            <Button type="submit" color="primary" >Add Locattion</Button>
          </ModalFooter>
        </Form>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    sites: state.firebase.data.sites,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getLocationsBySubscriptionIDAndBySiteID: LocationsActions.getLocationsBySubscriptionIDAndBySiteID,
    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID,

    sortDevicesBySiteID: DevicesActions.sortDevicesBySiteID,

  }, dispatch);
}

export default compose(
  firebaseConnect(),
  connect(
    mapStateToProps,
    mapDispatchToProps
))(SiteNewLocation);
