import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";

import * as SitesActions from '../../../_actions/sites';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';
import * as LocationsActions from '../../../_actions/locations';

const DeviceInformation = React.lazy(() => import( './DeviceInformation/DeviceInformation'));
const DeviceSpecifications = React.lazy(() => import( './DeviceSpecifications/DeviceSpecifications'));
const DeviceStatus = React.lazy(() => import( './DeviceStatus/DeviceStatus'));

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

class SiteDevice extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id, deviceID } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    const device = this.props.getDeviceByID(deviceID);
    // const camera = this.props.getCameraByID(cameraID);
    const devices = this.props.getDevicesBySubscriptionID(subscriptionID);
    const cameras = this.props.getCamerasBySubscriptionID(subscriptionID);
    const screens = this.props.getScreensBySubscriptionID(subscriptionID);
    
    const locations = this.props.getLocationsBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <div className="animated fadeIn d-flex flex-column" style={{width: '100%'}}>

        <Helmet>
          <title>{`Viana: ${subscriptionName} - Sites - ${site.name}`}</title>
        </Helmet>

        <Row className="mb-4">
          <Col>
            <h4 className=""><NavLink to={`/manage/sites/${id}/devices`}>Devices</NavLink> / {device.name}</h4>
          </Col>
          <Col>
            <Form className="float-right form-inline">
              <FormGroup className="float-right">
                <a className="btn btn-outline-primary text-primary disabled mr-2"><i className="fa fa-pen mr-2"></i>Edit Device</a>
                <a className="btn btn-outline-danger text-danger disabled"><i className="fa fa-trash mr-2"></i>Remove Device</a>
              </FormGroup>
            </Form>
          </Col>
        </Row>

        <Row>
          <Col xs={12} sm={12} md={12} lg ={8} xl={4}>
            <React.Suspense fallback={loading()}>
              <DeviceInformation {...this.props} />
            </React.Suspense>
            <React.Suspense fallback={loading()}>
              <DeviceSpecifications {...this.props} />
            </React.Suspense>
            <React.Suspense fallback={loading()}>
              <DeviceStatus {...this.props} />
            </React.Suspense>
          </Col>
          
          <Col>
            {/* <React.Suspense fallback={loading()}>
              <LocationProfile {...this.props} />
            </React.Suspense> */}
          </Col>
        </Row>

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,
    getCameraByID: CamerasActions.getCameraByID,
    getDeviceByID: DevicesActions.getDeviceByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionID: DevicesActions.getDevicesBySubscriptionID,
    getCamerasBySubscriptionID: CamerasActions.getCamerasBySubscriptionID,
    getScreensBySubscriptionID: ScreensActions.getScreensBySubscriptionID,

    getLocationsBySubscriptionIDAndBySiteID: LocationsActions.getLocationsBySubscriptionIDAndBySiteID,
    getDevicesBySubscriptionIDAndBySiteIDAndByLocationID: DevicesActions.getDevicesBySubscriptionIDAndBySiteIDAndByLocationID,
    getCamerasBySubscriptionIDAndBySiteIDAndByLocationID: CamerasActions.getCamerasBySubscriptionIDAndBySiteIDAndByLocationID,
    getScreensBySubscriptionIDAndBySiteIDAndByLocationID: ScreensActions.getScreensBySubscriptionIDAndBySiteIDAndByLocationID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiteDevice);
