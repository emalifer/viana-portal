import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";

import * as SitesActions from '../../../../_actions/sites';
import * as LocationsActions from '../../../../_actions/locations';
import * as DevicesActions from '../../../../_actions/devices';
import * as CamerasActions from '../../../../_actions/cameras';
import * as ScreensActions from '../../../../_actions/screens';

class DeviceSpecifications extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id, cameraID } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    const camera = this.props.getCameraByID(cameraID);
    const devices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);
    const cameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);
    const screens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, id);

    const cameraLocation = Object.assign({}, this.props.getLocationByID(camera.locationID));
    // const cameraLocation = Object.assign({}, this.props.getLocationByID(camera.locationID));

    return (
      <div className="animated fadeIn ">
        <div className="card callout callout-info mt-0">
          <h5 className="pt-3 mb-3">Device Specifications</h5>
          <table className="table table-borderless mb-3">
            <tbody>
              <tr>
                <th className="text-muted">Operating System</th>
                <th className="text-right"><span className="text-muted">NA</span></th>
              </tr>
              <tr>
                <th className="text-muted">Processor</th>
                <th className="text-right"><span className="text-muted">NA</span></th>
              </tr>
              <tr>
                <th className="text-muted">Viana Version</th>
                <th className="text-right"><span className="text-muted">NA</span></th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,
    getCameraByID: CamerasActions.getCameraByID,
    getLocationByID: LocationsActions.getLocationByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID,

    getDevicesBySubscriptionIDAndBySiteIDAndByLocationID: DevicesActions.getDevicesBySubscriptionIDAndBySiteIDAndByLocationID,
    getCamerasBySubscriptionIDAndBySiteIDAndByLocationID: CamerasActions.getCamerasBySubscriptionIDAndBySiteIDAndByLocationID,
    getScreensBySubscriptionIDAndBySiteIDAndByLocationID: ScreensActions.getScreensBySubscriptionIDAndBySiteIDAndByLocationID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceSpecifications);
