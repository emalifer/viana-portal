import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";

import * as SitesActions from '../../../../_actions/sites';
import * as LocationsActions from '../../../../_actions/locations';
import * as DevicesActions from '../../../../_actions/devices';
import * as CamerasActions from '../../../../_actions/cameras';
import * as ScreensActions from '../../../../_actions/screens';

class DeviceInformation extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id, deviceID } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    // const camera = this.props.getCameraByID(cameraID);
    const device = this.props.getDeviceByID(deviceID);
    const devices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);
    const cameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);
    const screens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, id);

    const deviceLocation = Object.assign({}, this.props.getLocationByID(device.locationID));
    // const cameraLocation = Object.assign({}, this.props.getLocationByID(camera.locationID));

    return (
      <div className="animated fadeIn ">
        <div className="card callout callout-info mt-0">
          <h5 className="pt-3 mb-3">Device Information</h5>
          <table className="table table-borderless mb-3">
            <tbody>
              <tr>
                <th className="text-muted">Name</th>
                <th className="text-right">{device.name}</th>
              </tr>
              <tr>
                <th className="text-muted">Installed on</th>
                <th className="text-right"><span className="text-muted">NA</span></th>
              </tr>
              <tr>
                <th className="text-muted">Serial #</th>
                <th className="text-right"><span className="text-muted">NA</span></th>
              </tr>
              <tr>
                <th className="text-muted">Assigned Site</th>
                <th className="text-right">{site.name}</th>
              </tr>
              <tr>
                <th className="text-muted">Assigned Location</th>
                <th className="text-right"><NavLink to={`/manage/sites/${id}/locations/${deviceLocation.id}`}>{deviceLocation.name}</NavLink></th>
              </tr>
              <tr>
                <th className="text-muted" colSpan="2">Description</th>
              </tr>
              <tr>
                <th className="text-left" colSpan="2">
                  {device.description}
                </th>
              </tr>
              <tr>
                <th className="text-muted">Licenses</th>
                <th className="text-right text-muted">
                  NA
                </th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,
    getCameraByID: CamerasActions.getCameraByID,
    getDeviceByID: DevicesActions.getDeviceByID,
    getLocationByID: LocationsActions.getLocationByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID,

    getDevicesBySubscriptionIDAndBySiteIDAndByLocationID: DevicesActions.getDevicesBySubscriptionIDAndBySiteIDAndByLocationID,
    getCamerasBySubscriptionIDAndBySiteIDAndByLocationID: CamerasActions.getCamerasBySubscriptionIDAndBySiteIDAndByLocationID,
    getScreensBySubscriptionIDAndBySiteIDAndByLocationID: ScreensActions.getScreensBySubscriptionIDAndBySiteIDAndByLocationID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceInformation);
