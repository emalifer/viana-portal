import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";

import * as SitesActions from '../../../../_actions/sites';
import * as DevicesActions from '../../../../_actions/devices';
import * as CamerasActions from '../../../../_actions/cameras';
import * as ScreensActions from '../../../../_actions/screens';

class LocationProfile extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id, locationID } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    const location = this.props.getLocationByID(locationID);
    const devices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);
    const cameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);
    const screens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, id);

    const locationDevices = this.props.getDevicesBySubscriptionIDAndBySiteIDAndByLocationID(subscriptionID, id, locationID);
    const locationCameras = this.props.getCamerasBySubscriptionIDAndBySiteIDAndByLocationID(subscriptionID, id, locationID);
    const locationScreens = this.props.getScreensBySubscriptionIDAndBySiteIDAndByLocationID(subscriptionID, id, locationID);

    return (
      <div className="animated fadeIn ">
        <div className="card callout callout-info mt-0">
          <h5 className="pt-3 mb-3">Location Profile</h5>
          <table className="table table-borderless mb-3">
            <tbody>
              <tr>
                <th className="text-muted">Name</th>
                <th className="text-right">{location.name}</th>
              </tr>
              <tr>
                <th className="text-muted">Description</th>
                <th className="text-right">
                  {location.description}
                </th>
              </tr>
              <tr>
                <th className="text-muted">Devices (<span className="text-primary">{Object.entries(locationDevices).length}</span>)</th>
                <th className="text-right">
                  {Object.entries(locationDevices).map(device => {
                    return <div key={`device-${device[0]}`}>{device[1].name}</div>
                  })}
                  {Object.entries(locationDevices).length === 0 &&
                    <span className="text-muted">None</span>
                  }
                </th>
              </tr>
              <tr>
                <th className="text-muted">Cameras (<span className="text-primary">{Object.entries(locationCameras).length}</span>)</th>
                <th className="text-right">
                  {Object.entries(locationCameras).map(camera => {
                    return <div key={`camera-${camera[0]}`}>{camera[1].name}</div>
                  })}
                  {Object.entries(locationCameras).length === 0 &&
                    <span className="text-muted">None</span>
                  }
                </th>
              </tr>
              <tr>
                <th className="text-muted">Screens (<span className="text-primary">{Object.entries(locationScreens).length}</span>)</th>
                <th className="text-right">
                  {Object.entries(locationScreens).map(screen => {
                    return <div key={`screen-${screen[0]}`}>{screen[1].name}</div>
                  })}
                  {Object.entries(locationScreens).length === 0 &&
                    <span className="text-muted">None</span>
                  }
                </th>
              </tr>
              <tr>
                <th className="text-muted">Licenses</th>
                <th className="text-right text-muted">
                  NA
                </th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID,

    getDevicesBySubscriptionIDAndBySiteIDAndByLocationID: DevicesActions.getDevicesBySubscriptionIDAndBySiteIDAndByLocationID,
    getCamerasBySubscriptionIDAndBySiteIDAndByLocationID: CamerasActions.getCamerasBySubscriptionIDAndBySiteIDAndByLocationID,
    getScreensBySubscriptionIDAndBySiteIDAndByLocationID: ScreensActions.getScreensBySubscriptionIDAndBySiteIDAndByLocationID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LocationProfile);
