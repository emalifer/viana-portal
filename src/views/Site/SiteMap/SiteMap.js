import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";

import * as SitesActions from '../../../_actions/sites';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';

class SiteMap extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    const devices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);
    const cameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);
    const screens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <div className="animated fadeIn ">
        <div className="mb-1">
          <Row>
            <Col xs="2" sm="2" md="2" lg="2" xl="2" >
              <h5 style={{lineHeight: '35px'}}>Site Map</h5>
            </Col>
            <Col className="text-right">
              <div className="d-inline-block">
                <form className="form-inline">
                  <div className="mr-4">Toggle View </div>
                  <div className="form-check form-check-inline">
                    <input className="form-check-input" type="radio" name="siteMapToggleView" id="siteMapToggleView-locations" value="locations" />
                    <label className="form-check-label" htmlFor="siteMapToggleView-locations">Locations</label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input className="form-check-input" disabled type="radio" name="siteMapToggleView" id="siteMapToggleView-devices" value="devices" />
                    <label className="form-check-label" htmlFor="siteMapToggleView-devices">Devices</label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input className="form-check-input" type="radio" name="siteMapToggleView" id="siteMapToggleView-cameras" value="cameras" />
                    <label className="form-check-label" htmlFor="siteMapToggleView-cameras">Cameras</label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input className="form-check-input" disabled type="radio" name="siteMapToggleView" id="siteMapToggleView-screens" value="screens" />
                    <label className="form-check-label" htmlFor="siteMapToggleView-screens">Screens</label>
                  </div>
                  <div className="form-group">
                    <select id="inputState" className="form-control disabled">
                      <option>Choose Location</option>
                      <option>Ground Floor</option>
                    </select>
                  </div>
                </form>
              </div>
            </Col>
          </Row>
        </div>
        <div className="card bg-light d-flex flex-column justify-content-center align-items-center" style={{height: '500px'}}>
          <i className="fa fa-upload mb-3" style={{fontSize: '1rem'}} />
          <div className="mb-1">Drag and drop to upload or <span className="text-primary">upload</span></div>
          <div className="">Max Size <span className="text-primary">10 MB</span></div>
        </div>  
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiteMap);
