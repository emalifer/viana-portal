import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet';
import {Helmet} from "react-helmet";
import { Doughnut, Bar } from 'react-chartjs-2';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import moment from 'moment';

import * as SitesActions from '../../../_actions/sites';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';

const otsData = {
  title: 'OTS',
  value: 17291,
  unit: 'Persons',
  format: '0,00',
  size: {
    xs: 12,
    sm: 6,
    md: 6,
    lg: 6,
    xl: 6
  },
  type: 'Doughnut',
  labels: ['OTS', 'Watchers'],
  datasets: [
    {
      label: 'OTS',
      backgroundColor: ['#0099A7', '#CCEBED'],
      borderColor: 'rgba(255,255,255,.55)',
      data: [75, 25],
    },
  ]
};

const dwelltimeattentionData = {
  title: 'OTS',
  value: 17291,
  unit: 'Persons',
  format: '0,00',
  size: {
    xs: 12,
    sm: 6,
    md: 6,
    lg: 6,
    xl: 6
  },
  type: 'Doughnut',
  labels: ['Dwell Time', 'Attention'],
  datasets: [
    {
      label: 'OTS',
      backgroundColor: ['#9C6ADE', '#E3D0FF'],
      borderColor: 'rgba(255,255,255,.55)',
      data: [75, 25],
    },
  ],
};

const doughnutOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: true,
  legend: {
    display: true,
    position: 'right',
  }
};

class SiteActivity extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentTime: moment()
    }

    this.timeInterval = null;
  }

  componentDidMount = () => {
    this.setTime();
  }

  setTime = () => {
    this.timeInterval = setInterval(() => {
      this.setState({currentTime: moment()})
    }, 500);
  }

  componentWillUnmount = () => {
    clearInterval(this.timeInterval)
  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const { currentTime } = this.state;

    const site = this.props.getSiteByID(id);
    const devices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);
    const cameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);
    const screens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <div className="animated fadeIn ">
        <div className="card callout callout-info mt-0">
          <h5 className="pt-3 mb-3">Site Activity</h5>
          <Row className="mb-3">
            <Col className="mb-3" xs="12" sm="12" md="6" lg="2" xl="2">
              <h6 className="text-muted">Today</h6>
              <h4 className="">{currentTime.format('h:mm a')}</h4>
              <h6 className="text-muted mb-4">{currentTime.format('ddd, MMMM DD')}</h6>
              <i className="text-muted fas fa-cloud-sun-rain mb-3" style={{fontSize: '4rem'}}></i>
              <h4 className="">24&deg;</h4>
            </Col>
            <Col className="mb-3" xs="12" sm="12" md="6" lg="4" xl="4">
              <h6 className="text-muted">OTS and Watchers</h6>
              <h4 className="mb-3">75% Conversion Ratio</h4>
              <Doughnut data={otsData} options={doughnutOpts} height={150} />
              <h4 className="mt-3 mb-0">32,992</h4>
              <h6>OTS</h6>
              <h4 className="mb-0">451</h4>
              <h6 className="mb-0">Watchers</h6>
            </Col>
            <Col className="mb-3" xs="12" sm="12" md="6" lg="4" xl="4">
              <h6 className="text-muted">Dwell time and attention</h6>
              <h4 className="mb-3">75% Attention Ratio</h4>
              <Doughnut data={dwelltimeattentionData} options={doughnutOpts} height={150} />
              <h4 className="mt-3 mb-0">3m 29s</h4>
              <h6>Dwell Time</h6>
              <h4 className="mb-0">1m 32s</h4>
              <h6 className="mb-0">Attention Time</h6>
            </Col>
            <Col className="mb-3" xs="12" sm="12" md="6" lg="2" xl="2">
              <h6 className="text-muted">Top Persona</h6>
              <h4 className="mb-3">Adult Male</h4>
              <i className="text-primary fas fa-user-alt" style={{fontSize: '4rem'}}></i>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiteActivity);
