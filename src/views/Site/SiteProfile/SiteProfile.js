import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";

import * as SitesActions from '../../../_actions/sites';
import * as LocationsActions from '../../../_actions/locations';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';

class SiteProfile extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    const locations = this.props.getLocationsBySubscriptionIDAndBySiteID(subscriptionID, id);
    const devices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);
    const cameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);
    const screens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <div className="animated fadeIn ">
        <div className="card callout callout-info mt-0">
          <h5 className="pt-3 mb-3">Site Profile</h5>
          <img className="img-fluid mb-3" src={`/assets/${site.image}`} alt={site.name} />
          <table className="table table-borderless mb-3">
            <tbody>
              <tr>
                <th className="text-muted">Name</th>
                <th className="text-right">{site.name}</th>
              </tr>
              <tr>
                <th className="text-muted">Address</th>
                <th className="text-right">
                  {site.location &&
                    <>
                      {site.location.street}<br/>
                      {site.location.suburb} {site.location.state} {site.location.postCode}<br/>
                      {site.location.country}
                    </>
                  }
                </th>
              </tr>
              <tr>
                <th className="text-muted">Locations (<NavLink to={`/manage/sites/${id}/locations`} className="">{Object.entries(locations).length}</NavLink>)</th>
                <th className="text-right">
                  {/* {locations.map(location => {
                    return <NavLink to={`/manage/sites/${id}/locations/${location.id}`} key={`location-${location.id}`}>{location.name}</NavLink>
                  })}
                  {locations.length === 0 &&
                    <span className="text-muted">None</span>
                  } */}
                  {Object.entries(locations).map(location => {
                    return <React.Fragment key={`${location[0]}`}><NavLink to={`/manage/sites/${id}/locations/${location[0]}`} key={`location-${location[0]}`}>{location[1].name}</NavLink><br/></React.Fragment>
                  })}
                  {Object.entries(locations).length === 0 &&
                    <span className="text-muted">None</span>
                  }
                </th>
              </tr>
              <tr>
                <th className="text-muted">Devices (<NavLink to={`/manage/sites/${id}/devices`} className="text-muted disabled">{Object.entries(devices).length}</NavLink>)</th>
                <th className="text-right">
                  {Object.entries(devices).map(device => {
                    return <React.Fragment key={`${device[0]}`}><div key={`device-${device[0]}`}>{device[1].name}</div><br/></React.Fragment>
                  })}
                  {Object.entries(devices).length === 0 &&
                    <span className="text-muted">None</span>
                  }
                </th>
              </tr>
              <tr>
                <th className="text-muted">Cameras (<NavLink to={`/manage/sites/${id}/cameras`} className="">{Object.entries(cameras).length}</NavLink>)</th>
                <th className="text-right">
                  {Object.entries(cameras).map(camera => {
                    return <React.Fragment key={`${camera[0]}`}><NavLink key={`camera-${camera[0]}`} to={`/manage/sites/${id}/cameras/${camera[0]}`}>{camera[1].name}</NavLink><br/></React.Fragment>
                  })}
                  {Object.entries(cameras).length === 0 &&
                    <span className="text-muted">None</span>
                  }
                </th>
              </tr>
              <tr>
                <th className="text-muted">Screens (<NavLink to={`/manage/sites/${id}/screens`} className="text-muted disabled">{Object.entries(screens).length}</NavLink>)</th>
                <th className="text-right">
                  {Object.entries(screens).map(screen => {
                    return <React.Fragment key={`${screen[0]}`}><div key={`screen-${screen[0]}`}>{screen[1].name}</div><br/></React.Fragment>
                  })}
                  {Object.entries(screens).length === 0 &&
                    <span className="text-muted">NA</span>
                  }
                </th>
              </tr>
              <tr>
                <th className="text-muted">Licenses</th>
                <th className="text-right text-muted">
                  NA
                </th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,
    getLocationsBySubscriptionIDAndBySiteID: LocationsActions.getLocationsBySubscriptionIDAndBySiteID,
    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiteProfile);
