import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";

import * as SitesActions from '../../../_actions/sites';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';
import * as LocationsActions from '../../../_actions/locations';

class SiteLocations extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    const devices = this.props.getDevicesBySubscriptionID(subscriptionID);
    const cameras = this.props.getCamerasBySubscriptionID(subscriptionID);
    const screens = this.props.getScreensBySubscriptionID(subscriptionID);

    const siteLocations = this.props.getLocationsBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <div className="animated fadeIn d-flex flex-column" style={{width: '100%'}}>

        <Helmet>
          <title>{`Viana: ${subscriptionName} - Sites - ${site.name}`}</title>
        </Helmet>

        <Row className="mb-4">
          <Col>
            <h4 className="">All Locations <span className="badge badge-pill badge-primary ml-2">{Object.entries(siteLocations).length}</span></h4>
          </Col>
          <Col>
            <Form className="float-right form-inline">
              <FormGroup className="float-right">
                <Input type="text" name="locations" id="locations" placeholder="Search Locations" className="mr-2 form-control" disabled />
                <a className="btn btn-outline-primary text-primary disabled"><i className="fa fa-plus mr-2"></i>Filter Results</a>
              </FormGroup>
            </Form>
          </Col>
        </Row>

        <table className="table">
          <thead>
            <tr>
              <th scope="col">Location Name</th>
              <th scope="col">Description</th>
              <th scope="col">Device Count</th>
              <th scope="col">Camera Count</th>
              <th scope="col">Screen Count</th>
              <th scope="col" width="150"></th>
            </tr>
          </thead>
          {Object.entries(siteLocations).length > 0 &&
            <tbody>
              {Object.entries(siteLocations).map(location => {

                const locationDevices = this.props.getDevicesBySubscriptionIDAndBySiteIDAndByLocationID(subscriptionID, id, location[0]);
                const locationCameras = this.props.getCamerasBySubscriptionIDAndBySiteIDAndByLocationID(subscriptionID, id, location[0]);
                const locationScreens = this.props.getScreensBySubscriptionIDAndBySiteIDAndByLocationID(subscriptionID, id, location[0]);

                return (
                  <tr key={`location-${location[0]}`}>
                    <td scope="col"><NavLink className="" exact to={`/manage/sites/${id}/locations/${location[0]}`}>{location[1].name}</NavLink></td>
                    <td scope="col">{location[1].description}</td>
                    <td scope="col">{Object.entries(locationDevices).length}</td>
                    <td scope="col">{Object.entries(locationCameras).length}</td>
                    <td scope="col">{Object.entries(locationScreens).length}</td>
                    <td scope="col">
                      <NavLink className="mr-3" exact to={`/manage/sites/${id}/locations/${location[0]}`}>View Profile <i className="ml-2 fas fa-arrow-circle-right" /></NavLink>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          }
        </table>

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionID: DevicesActions.getDevicesBySubscriptionID,
    getCamerasBySubscriptionID: CamerasActions.getCamerasBySubscriptionID,
    getScreensBySubscriptionID: ScreensActions.getScreensBySubscriptionID,

    getLocationsBySubscriptionIDAndBySiteID: LocationsActions.getLocationsBySubscriptionIDAndBySiteID,
    getDevicesBySubscriptionIDAndBySiteIDAndByLocationID: DevicesActions.getDevicesBySubscriptionIDAndBySiteIDAndByLocationID,
    getCamerasBySubscriptionIDAndBySiteIDAndByLocationID: CamerasActions.getCamerasBySubscriptionIDAndBySiteIDAndByLocationID,
    getScreensBySubscriptionIDAndBySiteIDAndByLocationID: ScreensActions.getScreensBySubscriptionIDAndBySiteIDAndByLocationID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiteLocations);
