import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";

import * as SitesActions from '../../../_actions/sites';
import * as LocationsActions from '../../../_actions/locations';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';

class SiteCameras extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
  }

  componentDidUpdate = () => {

  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;

    const site = this.props.getSiteByID(id);
    const devices = this.props.getDevicesBySubscriptionID(subscriptionID);
    const cameras = this.props.getCamerasBySubscriptionID(subscriptionID);
    const screens = this.props.getScreensBySubscriptionID(subscriptionID);

    const siteCameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <div className="animated fadeIn d-flex flex-column" style={{width: '100%'}}>

        <Helmet>
          <title>{`Viana: ${subscriptionName} - Sites - ${site.name}`}</title>
        </Helmet>

        <Row className="mb-4">
          <Col>
            <h4 className="">All Cameras <span className="badge badge-pill badge-primary ml-2">{Object.entries(siteCameras).length}</span></h4>
          </Col>
          <Col>
            <Form className="float-right form-inline">
              <FormGroup className="float-right">
                <Input type="text" name="locations" id="locations" placeholder="Search Cameras" className="mr-2 form-control" disabled />
                <a className="btn btn-outline-primary text-primary disabled"><i className="fa fa-plus mr-2"></i>Filter Results</a>
              </FormGroup>
            </Form>
          </Col>
        </Row>

        <table className="table">
          <thead>
            <tr>
              <th scope="col">Camera Name</th>
              <th scope="col">Location</th>
              <th scope="col">Device</th>
              <th scope="col" width="280"></th>
            </tr>
          </thead>
          {Object.entries(siteCameras).length > 0 &&
            <tbody>
              {Object.entries(siteCameras).map(camera => {

                const cameraLocation = this.props.getLocationByID(camera[1].locationID);
                const cameraDevice= this.props.getDeviceByID(camera[1].deviceID);

                return (
                  <tr key={`location-${camera[0]}`}>
                    <td scope="col"><NavLink className="" exact to={`/manage/sites/${id}/cameras/${camera[0]}`}>{camera[1].name}</NavLink></td>
                    <td scope="col"><NavLink className="" exact to={`/manage/sites/${id}/locations/${camera[1].locationID}`}>{cameraLocation.name}</NavLink></td>
                    <td scope="col"><NavLink className="" exact to={`/manage/sites/${id}/devices/${camera[1].deviceID}`}>{cameraDevice.name}</NavLink></td>
                    <td scope="col">
                      <NavLink className="mr-3" exact to={`/manage/sites/${id}/cameras/${camera[0]}`}>View Profile <i className="ml-2 fas fa-arrow-circle-right" /></NavLink>
                      <NavLink className="mr-3 disabled" exact to={`/manage/sites/${id}/cameras/${camera[0]}/live`}>View Live Feed <i className="ml-2 fas fa-arrow-circle-right" /></NavLink>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          }
        </table>

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,
    getDeviceByID: DevicesActions.getDeviceByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionID: DevicesActions.getDevicesBySubscriptionID,
    getCamerasBySubscriptionID: CamerasActions.getCamerasBySubscriptionID,
    getScreensBySubscriptionID: ScreensActions.getScreensBySubscriptionID,

    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getLocationByID: LocationsActions.getLocationByID

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiteCameras);
