import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Label, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";
import TreeView from 'react-treeview';
import { firebaseConnect } from 'react-redux-firebase';

import * as SitesActions from '../../../_actions/sites';
import * as LocationsActions from '../../../_actions/locations';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';

class SiteNewCamera extends Component {
  constructor(props) {
    super(props);
  }

  addCamera = (event) => {
    event.preventDefault();

    console.log('add camera');
    const { subscriptionID } = this.props.auth;

    this.props.firebase.push('/cameras', 
      {
        name: event.target.name.value,
        description: event.target.description.value,
        siteID: event.target.siteID.value,
        deviceID: event.target.deviceID.value,
        locationID: event.target.locationID.value,
        subscriptionID 
      });
    this.props.toggle();
  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;
    const { isOpen, toggle, className } = this.props; 

    const site = this.props.getSiteByID(id);
    const sites = this.props.getSitesBySubscriptionID(subscriptionID, id);
    const locations = this.props.getLocationsBySubscriptionIDAndBySiteID(subscriptionID, id);
    const devices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);
    const cameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);
    const screens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <Modal isOpen={isOpen} toggle={toggle} size={'md'} centered className={className}>
        <Form onSubmit={(e) => { this.addCamera(e); }}>
          <ModalHeader toggle={toggle}>Add New Camera</ModalHeader>
          <ModalBody>
            <Row>
              <Col>
                <FormGroup>
                  <Label htmlFor="name">Camera Name <span className="text-danger">*</span></Label>
                  <Input type="text" id="name" name="name" placeholder="Camera Name" required />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label htmlFor="networkID" className="text-light">Assigned Network</Label>
                  <select className="form-control disabled">
                  </select>
                </FormGroup>
              </Col>
              <Col></Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label htmlFor="siteID">Assigned Site <span className="text-danger">*</span></Label>
                  <select id="siteID" name="siteID" className="form-control">
                    {Object.entries(sites).map((site) => {
                      return (
                        <option key={site[0]} value={site[0]}>{site[1].name}</option>
                      )
                    })}
                  </select>
                </FormGroup>
              </Col>
              <Col>
                <FormGroup>
                  <Label htmlFor="deviceID">Assigned Device <span className="text-danger">*</span></Label>
                  <select id="deviceID" name="deviceID" className="form-control">
                    <option value={''}>Unassign</option>
                    {Object.entries(devices).map((device) => {
                      return (
                        <option key={device[0]} value={device[0]}>{device[1].name}</option>
                      )
                    })}
                  </select>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label htmlFor="locationID">Assigned Location <span className="text-danger">*</span></Label>
                  <select id="locationID" name="locationID" className="form-control">
                    <option value={''}>Unassign</option>
                    {Object.entries(locations).map((location) => {
                      return (
                        <option key={location[0]} value={location[0]}>{location[1].name}</option>
                      )
                    })}
                  </select>
                </FormGroup>
              </Col>
              <Col>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                    <Label htmlFor="description">Camera Description</Label>
                    <textarea id="description" name="description" rows="5" className="form-control">
                    </textarea>
                </FormGroup>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button color="light" onClick={toggle}>Cancel</Button>
            <Button type="submit" color="primary" >Add Camera</Button>
          </ModalFooter>
        </Form>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    sites: state.firebase.data.sites,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getLocationsBySubscriptionIDAndBySiteID: LocationsActions.getLocationsBySubscriptionIDAndBySiteID,
    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID,

    sortDevicesBySiteID: DevicesActions.sortDevicesBySiteID,

  }, dispatch);
}

export default compose(
  firebaseConnect(),
  connect(
    mapStateToProps,
    mapDispatchToProps
))(SiteNewCamera);
