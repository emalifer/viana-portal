import React, { Component, lazy, Suspense } from 'react';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";
import L from 'leaflet';

import * as SitesActions from '../../_actions/sites';
import * as DevicesActions from '../../_actions/devices';
import * as CamerasActions from '../../_actions/cameras';
import * as ScreensActions from '../../_actions/screens';
import icon from '../../assets/img/marker.png';
// import iconShadow from 'leaflet/dist/images/marker-shadow.png';

let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: null,
    iconSize: [30,30],
    iconAnchor: [15,30],
    shadowAnchor: [15,40]
});

L.Marker.prototype.options.icon = DefaultIcon;

class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {

    const { subscriptionID, subscriptionName, profile, subscriptionImage } = this.props.auth;

    const sites = this.props.getSitesBySubscriptionID(subscriptionID);
    const devices = this.props.getDevicesBySubscriptionID(subscriptionID);
    const cameras = this.props.getCamerasBySubscriptionID(subscriptionID);
    const screens = this.props.getScreensBySubscriptionID(subscriptionID);

    return (
      <div className="animated fadeIn d-flex flex-column" style={{width: '100%'}}>

        <Helmet>
          <title>Viana: {subscriptionName} - Home</title>
        </Helmet>

        <h3 className="page-title mb-4"><img src={`/assets/img/${subscriptionImage}`} width="30" className="mr-3" /><span className="d-inline-block" style={{verticalAlign: 'middle'}}>{subscriptionName}</span></h3>

        <Row className="d-flex mb-4" style={{flex: 1}}>

          <Col className="d-flex" xs="12" sm="12" md="12" lg="6" xl="6">
            <Card className="site-map mb-0" style={{flex: 1}}>
              <CardBody className="p-0 overflow-hidden" style={{height: '100%'}}>
                <h5 className="p-3" style={{position: 'absolute', zIndex: 1000}}>Site Map</h5>

                <Map center={[-33.818102,151.0052755]} zoom={10} zoomControl={false} style={{width: '100%', display: 'block !important', height: '100%'}}>
                  <ZoomControl position="bottomright" />
                  <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
                    url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                  />
                  {Object.entries(sites).map((site) => {

                    const siteID = site[0];
                    const siteDevices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, siteID);
                    const siteCameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, siteID);
                    const siteScreens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, siteID);

                    return (
                      <Marker position={[site[1].location.coordinates.lat, site[1].location.coordinates.lng]} key={`marker-${site[0]}`}>
                        <Popup>
                          <h4 className="text-primary mb-1">{site[1].name}</h4>
                          <p className="mt-0 mb-3">{site[1].location.street}, {site[1].location.suburb} {site[1].location.state} {site[1].location.postCode}, {site[1].location.country}</p>
                          <Row className="mb-3">
                            <Col>
                              <img className="img-fluid" src={`/assets/${site[1].image}`} alt={site[1].name} />
                            </Col>
                            <Col>
                              <p className="mb-2 mt-0"><i className="cil-laptop mr-2"></i>{Object.keys(siteDevices).length} Device{Object.keys(siteDevices).length === 1 ? `` : `s`}</p>
                              <p className="mb-2 mt-0"><i className="cil-camera mr-2"></i>{Object.keys(siteCameras).length} Camera{Object.keys(siteCameras).length === 1 ? `` : `s`}</p>
                              <p className="mb-2 mt-0"><i className="cil-devices mr-2"></i>{Object.keys(siteScreens).length} Screen{Object.keys(siteScreens).length === 1 ? `` : `s`}</p>
                            </Col>
                          </Row>
                          <Row>
                            <Col>
                              <NavLink to={`/manage/sites/${site[0]}`} className="btn btn-primary btn-block text-white">
                                Go to Site <i className="fas fa-arrow-circle-right ml-2" />
                              </NavLink>
                            </Col>
                          </Row>
                        </Popup>
                      </Marker>
                    )
                  })}
                </Map>
              </CardBody>
            </Card>
          </Col>

          <Col>

            <Card className="callout callout-info overflow-hidden mt-0">
              <CardBody>
                <h5 className="mb-2">Notifications</h5>
                <h6 className="text-muted mb-3">Looks like you've got some things to check on.</h6>
                <ul style={{lineHeight: '2rem'}}>
                  <li>3 of your licenses have <span className="text-danger font-weight-bold">expired</span>, renew them <a className="text-primary font-weight-bold">here</a>.</li>
                  <li>3 sites have <span className="text-danger font-weight-bold">incomplete addresses</span>, complete them <a className="text-primary font-weight-bold">here</a>.</li>
                  <li>Device AU-CH-DEV-003 has been <span className="text-danger font-weight-bold">offline</span> since 12/12/19, troubleshoot <a className="text-primary font-weight-bold">here</a>.</li>
                </ul>
              </CardBody>
              <div className=" mx-3" style={{ height: '30px' }}>
                <i className="far fa-user-circle" style={{fontSize: '100px', right: '-10px', bottom: '-10px', position: 'absolute', opacity: 0.1}}></i>
              </div>
            </Card>

            <Row>
              <Col>
                  <Card className="callout callout-info overflow-hidden mt-0">
                    <CardBody className="">
                      <h5 className="mb-4">{subscriptionName}</h5>
                      <h5 className="mb-3"><i className="cil-location-pin mr-2"></i><span className="text-success">{Object.keys(sites).length}</span> Site{Object.keys(sites).length === 1 ? `` : `s`}</h5>
                      <h5 className="mb-3"><i className="cil-laptop mr-2"></i><span className="text-success">{Object.keys(devices).length}</span> Device{Object.keys(devices).length === 1 ? `` : `s`}</h5>
                      <h5 className="mb-3"><i className="cil-camera mr-2"></i><span className="text-success">{Object.keys(cameras).length}</span> Camera{Object.keys(cameras).length === 1 ? `` : `s`}</h5>
                      <h5 className="mb-3"><i className="cil-devices mr-2"></i><span className="text-success">{Object.keys(screens).length}</span> Screen{Object.keys(screens).length === 1 ? `` : `s`}</h5>
                    </CardBody>
                  </Card>
              </Col>
            </Row>


          </Col>

        </Row>

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    sites: state.firebase.data.sites,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionID: DevicesActions.getDevicesBySubscriptionID,
    getCamerasBySubscriptionID: CamerasActions.getCamerasBySubscriptionID,
    getScreensBySubscriptionID: ScreensActions.getScreensBySubscriptionID,

    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID

  }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

