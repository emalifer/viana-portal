import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

import logo from '../../../assets//img/brand/logo_blue.png';
import image from '../../../assets/img/14148162965_60ca46af0c_b.jpg'

class Login extends Component {

  login = () => {
    this.props.history.push('/home');
  }


  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container className="vh-100 align-items-end d-none d-lg-flex text-white" style={{backgroundImage: `url(${image})`, backgroundSize: 'cover', backgroundPosition: 'left'}}>
        <p style={{fontSize: '0.9rem', fontStyle: 'italic'}}>
          <a className="text-muted" href="https://www.flickr.com/photos/52159252@N02/14148162965">
            "Orlando airport"</a><span> by <a href="https://www.flickr.com/photos/52159252@N02" className="text-muted" >Margalit Francus</a></span> is licensed under 
          <a href="https://creativecommons.org/licenses/by-nc-sa/2.0/?ref=ccsearch&atype=html" style={{marginRight: '5px'}} className="text-muted" >CC BY-NC-SA 2.0</a>
        </p>
        </Container>
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <Row className="mb-3">
                <Col className="">
                  <img src={logo} width="100" />
                </Col>
              </Row>
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h3 className="text-primary text-center mb-4">Login to your account</h3>
                      <p className="text-muted mb-1">Email Address</p>
                      <InputGroup className="mb-3">
                        <Input type="text" placeholder="" autoComplete="username" />
                      </InputGroup>
                      <p className="text-muted mb-1">Password</p>
                      <InputGroup className="mb-4">
                        <Input type="password" placeholder="" autoComplete="current-password" />
                      </InputGroup>
                      <div className="form-check mb-3">
                        <input className="form-check-input" type="checkbox" value="" id="remember" />
                        <label className="form-check-label text-muted" htmlFor="remember">
                          Remember Me
                        </label>
                      </div>
                      <Row>
                        <Col>
                          <Button color="primary" className="px-4 btn-lg btn-block" onClick={() => { this.login(); }}>Login</Button>
                        </Col>
                      </Row>
                      <hr className="mt-3" />
                      <Row>
                        <Col>
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
              <Row className="mt-4">
                <Col className="text-center">
                  <Button color="link" className="px-0 mr-3">Privacy Policy</Button>
                  <Button color="link" className="px-0 ml-3">Terms &amp; Conditions</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
