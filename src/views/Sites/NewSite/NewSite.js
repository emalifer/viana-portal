import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Label, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";
import TreeView from 'react-treeview';
import { firebaseConnect } from 'react-redux-firebase';

import * as SitesActions from '../../../_actions/sites';
import * as LocationsActions from '../../../_actions/locations';
import * as DevicesActions from '../../../_actions/devices';
import * as CamerasActions from '../../../_actions/cameras';
import * as ScreensActions from '../../../_actions/screens';

class NewSite extends Component {
  constructor(props) {
    super(props);

    this.state = {
      coordinates: {
        lat: -33.818102,
        lng: 151.0052755,
      }
    }
  }

  markerClick = (e) => {
    this.setState(produce(
      this.state, draftState => {
        draftState.coordinates.lat = e.latlng.lat;
        draftState.coordinates.lng = e.latlng.lng;
      }
    ))
  }

  addSite = (event) => {
    event.preventDefault();

    console.log('add site');
    const { subscriptionID } = this.props.auth;

    this.props.firebase.push('/sites', 
      {
        name: event.target.name.value,
        location: {
          street: event.target.street.value,
          suburb: event.target.suburb.value,
          postCode: event.target.postCode.value,
          state: event.target.state.value,
          country: event.target.country.value,
          coordinates: {
            lat: this.state.coordinates.lat,
            lng: this.state.coordinates.lng
          }
        },
        description: event.target.description.value,
        image: 'sites/fc18c90b-a0e8-48a3-aa84-b8d95912e77a.jpg',
        subscriptionID 
      });
    this.props.toggle();
  }

  render() {

    const { id } = this.props.match.params;
    const { pathname } = this.props.location;
    const { subscriptionID, subscriptionName, profile } = this.props.auth;
    const { isOpen, toggle, className } = this.props; 
    const { coordinates } = this.state;

    const site = this.props.getSiteByID(id);
    const sites = this.props.getSitesBySubscriptionID(subscriptionID, id);
    const locations = this.props.getLocationsBySubscriptionIDAndBySiteID(subscriptionID, id);
    const devices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, id);
    const cameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, id);
    const screens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, id);

    return (
      <Modal isOpen={isOpen} toggle={toggle} size={'lg'} centered className={className}>
        <Form onSubmit={(e) => { this.addSite(e); }}>
          <ModalHeader toggle={toggle}>Add New Site</ModalHeader>
          <ModalBody>

            <Row>
              <Col>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label htmlFor="name">Site Name <span className="text-danger">*</span></Label>
                      <Input type="text" id="name" name="name" placeholder="Site Name" required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label htmlFor="street">Site Street <span className="text-danger">*</span></Label>
                      <Input type="text" id="street" name="street" placeholder="Site Street" required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label htmlFor="suburb">Site Suburb <span className="text-danger">*</span></Label>
                      <Input type="text" id="suburb" name="suburb" placeholder="Site Suburb" required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label htmlFor="postCode">Site Post Code <span className="text-danger">*</span></Label>
                      <Input type="text" id="postCode" name="postCode" placeholder="Site Post Code" required />
                    </FormGroup>
                  </Col>
                  <Col>
                    <FormGroup>
                      <Label htmlFor="state">Site State <span className="text-danger">*</span></Label>
                      <Input type="text" id="state" name="state" placeholder="Site State" required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label htmlFor="country">Site Country <span className="text-danger">*</span></Label>
                      <Input type="text" id="country" name="country" placeholder="Site Country" required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormGroup>
                        <Label htmlFor="description">Site Description</Label>
                        <textarea id="description" name="description" rows="6" className="form-control">
                        </textarea>
                    </FormGroup>
                  </Col>
                </Row>
              </Col>

              <Col>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label htmlFor="name">Site Photo <span className="text-danger"></span></Label>
                      <div className="bg-light d-flex flex-column justify-content-center align-items-center" style={{height: '250px'}}>
                        <i className="fa fa-upload mb-3" style={{fontSize: '1rem'}} />
                        <div className="mb-1">Drag and drop to upload or <span className="text-primary">upload</span></div>
                        <div className="">Max Size <span className="text-primary">10 MB</span></div>
                      </div>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <FormGroup>
                      <Label htmlFor="name">Site Location <span className="text-danger"></span></Label>
                      <Map center={[-33.818102,151.0052755]} zoom={10} zoomControl={false} style={{width: '100%', display: 'block !important', height: '250px'}} onClick={(e) => this.markerClick(e)}>
                        <ZoomControl position="bottomright" />
                        <TileLayer
                          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
                          url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                          ref={`tilelayer`}
                        />
                        <Marker position={[coordinates.lat, coordinates.lng]} key={`marker-newsite`} id={`marker-newsite`} ref={`marker-newsite`}></Marker>
                      </Map>
                    </FormGroup>
                  </Col>
                </Row>
              </Col>
            </Row>

            

          </ModalBody>
          <ModalFooter>
            <Button color="light" onClick={toggle}>Cancel</Button>
            <Button type="submit" color="primary" >Add Site</Button>
          </ModalFooter>
        </Form>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    sites: state.firebase.data.sites,
    locations: state.firebase.data.locations,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSiteByID: SitesActions.getSiteByID,

    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getLocationsBySubscriptionIDAndBySiteID: LocationsActions.getLocationsBySubscriptionIDAndBySiteID,
    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID,

    sortDevicesBySiteID: DevicesActions.sortDevicesBySiteID,

  }, dispatch);
}

export default compose(
  firebaseConnect(),
  connect(
    mapStateToProps,
    mapDispatchToProps
))(NewSite);
