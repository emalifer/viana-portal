import React, { Component, lazy, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import produce from 'immer';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, ButtonGroup, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup } from 'reactstrap';
import { Map, Marker, Popup, TileLayer, ZoomControl } from 'react-leaflet'
import {Helmet} from "react-helmet";
import L from 'leaflet';
import TreeView from 'react-treeview';

import * as SitesActions from '../../_actions/sites';
import * as DevicesActions from '../../_actions/devices';
import * as CamerasActions from '../../_actions/cameras';
import * as ScreensActions from '../../_actions/screens';
import icon from '../../assets/img/marker.png';
import NewSite from './NewSite/NewSite';
// import iconShadow from 'leaflet/dist/images/marker-shadow.png';

let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: null,
    iconSize: [30,30],
    iconAnchor: [15,30],
    shadowAnchor: [15,40]
});

L.Marker.prototype.options.icon = DefaultIcon;

class Sites extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sortedSitesByLocation: {},
      newSiteModal: false
    }
  }

  componentDidMount = () => {
    this.sortSitesByListing();
  }

  componentDidUpdate = (prevProps) => {
    if(prevProps.sites !== this.props.sites) {
      this.sortSitesByListing();
    }
  }

  sortSitesByListing = () => {
    this.setState({sortedSitesByLocation: this.props.sortSitesByLocation()});
  }

  toggle = (i = null, j = null) => {
    if(j != null) {
        this.setState(
          produce(this.state, draftState => {
            draftState.sortedSitesByLocation[i].states[j].collapsed = !this.state.sortedSitesByLocation[i].states[j].collapsed;
          })
        );
      } else {
        if(i != null) {
          this.setState(
            produce(this.state, draftState => {
              draftState.sortedSitesByLocation[i].collapsed = !this.state.sortedSitesByLocation[i].collapsed;
            })
          );
        }
      }
  }

  setSiteSelection = (siteIDx) => {
    this.refs['tilelayer'].leafletElement.closePopup();
    this.clearSiteSelection(() => {
      this.setState(
        produce(this.state, draftState => {
          for (const [countryID, countryValue] of Object.entries(draftState.sortedSitesByLocation)) {
            for (const [stateID, stateValue] of Object.entries(countryValue.states)) {
              for (const [siteID, siteValue] of Object.entries(stateValue.sites)) {
                if(siteID === siteIDx){
                  siteValue.collapsed = false;
                }
              }
            }
          }
        })
      , () => {
        this.refs['marker-' + siteIDx].leafletElement.openPopup();
      });
    })
  }

  clearSiteSelection = (cb = () => {}) => {
    this.setState(
      produce(this.state, draftState => {
        for (const [countryID, countryValue] of Object.entries(draftState.sortedSitesByLocation)) {
          for (const [stateID, stateValue] of Object.entries(countryValue.states)) {
            for (const [siteID, siteValue] of Object.entries(stateValue.sites)) {
              siteValue.collapsed = true;
            }
          }
        }
      })
    , () => {
      for (const [countryID, countryValue] of Object.entries(this.state.sortedSitesByLocation)) {
        for (const [stateID, stateValue] of Object.entries(countryValue.states)) {
          for (const [siteID, siteValue] of Object.entries(stateValue.sites)) {
            this.refs['marker-' + siteID].leafletElement.closePopup();
          }
        }
      }
      cb();
    })
  }

  render() {

    const { sortedSitesByLocation } = this.state;
    const { subscriptionID, subscriptionName, subscriptionImage, profile } = this.props.auth;

    const sites = this.props.getSitesBySubscriptionID(subscriptionID);
    const devices = this.props.getDevicesBySubscriptionID(subscriptionID);
    const cameras = this.props.getCamerasBySubscriptionID(subscriptionID);
    const screens = this.props.getScreensBySubscriptionID(subscriptionID);

    return (
      <div className="animated fadeIn d-flex flex-column" style={{width: '100%'}}>

        <Helmet>
          <title>Viana: {subscriptionName} - Sites</title>
        </Helmet>

        <h3 className="mb-4 page-title"><img src={`/assets/img/${subscriptionImage}`} width="30" className="mr-3" /><span className="d-inline-block" style={{verticalAlign: 'middle'}}><NavLink to={`/home`}>{subscriptionName}</NavLink> / Sites</span>
        
          <Form className="float-right form-inline">
            <FormGroup className="float-right">
              <Input type="text" name="site" id="site" placeholder="Search Sites" className="mr-2 form-control-lg shadow-sm" disabled />
              <a className="btn btn-primary btn-lg text-white" onClick={() => this.setState({newSiteModal: !this.state.newSiteModal})}><i className="fa fa-plus mr-2"></i>New Site</a>
            </FormGroup>
          </Form>
        </h3>

        <Row className="d-flex mb-4" style={{flex: 1}}>

          <Col className="d-flex" xs="12" sm="12" md="12" lg="8" xl="8">
            <Card className="site-map mb-0" style={{flex: 1}}>
              <CardBody className="p-0 overflow-hidden" style={{height: '100%'}}>
                <h5 className="p-3" style={{position: 'absolute', zIndex: 1000}}>Site Map</h5>

                <Map center={[-33.818102,151.0052755]} zoom={10} zoomControl={false} style={{width: '100%', display: 'block !important', height: '100%'}}>
                  <ZoomControl position="bottomright" />
                  <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
                    url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                    ref={`tilelayer`}
                  />
                  {Object.entries(sites).map((site) => {

                    const siteID = site[0];
                    const siteDevices = this.props.getDevicesBySubscriptionIDAndBySiteID(subscriptionID, siteID);
                    const siteCameras = this.props.getCamerasBySubscriptionIDAndBySiteID(subscriptionID, siteID);
                    const siteScreens = this.props.getScreensBySubscriptionIDAndBySiteID(subscriptionID, siteID);

                    return (
                      <Marker position={[site[1].location.coordinates.lat, site[1].location.coordinates.lng]} key={`marker-${site[0]}`} id={`marker-${site[0]}`} ref={`marker-${site[0]}`} onClick={() => { this.setSiteSelection(site[0]) }}>
                        <Popup onClose={() => { this.clearSiteSelection(); }}>
                          <NavLink to={`/manage/sites/${site[0]}`}><h4 className="text-primary mb-1">{site[1].name}</h4></NavLink>
                          <p className="mt-0 mb-3">{site[1].location.street}, {site[1].location.suburb} {site[1].location.state} {site[1].location.postCode}, {site[1].location.country}</p>
                          <Row className="mb-3">
                            <Col>
                              <img className="img-fluid" src={`/assets/${site[1].image}`} alt={site[1].name} />
                            </Col>
                            <Col>
                              <p className="mb-2 mt-0"><i className="cil-laptop mr-2"></i>{Object.keys(siteDevices).length} Device{Object.keys(siteDevices).length === 1 ? `` : `s`}</p>
                              <p className="mb-2 mt-0"><i className="cil-camera mr-2"></i>{Object.keys(siteCameras).length} Camera{Object.keys(siteCameras).length === 1 ? `` : `s`}</p>
                              <p className="mb-2 mt-0"><i className="cil-devices mr-2"></i>{Object.keys(siteScreens).length} Screen{Object.keys(siteScreens).length === 1 ? `` : `s`}</p>
                            </Col>
                          </Row>
                          <Row>
                            <Col>
                              <NavLink to={`/manage/sites/${site[0]}`} className="btn btn-primary btn-block text-white">
                                Go to Site <i className="fas fa-arrow-circle-right ml-2" />
                              </NavLink>
                            </Col>
                          </Row>
                        </Popup>
                      </Marker>
                    )
                  })}
                </Map>
              </CardBody>
            </Card>
          </Col>

          <Col>

            <Card className="callout callout-info overflow-hidden mt-0">
              <CardBody>
                <h5 className="mb-2">Site Listing <span className="badge badge-pill badge-primary ml-2">{Object.entries(sites).length}</span></h5>
                <h6 className="text-muted mb-4">Select a site from the map or from the listing below</h6>

                <div style={{position: 'relative', zIndex: 1}}>

                  {Object.entries(sortedSitesByLocation).map((country) => {
                    const countryLabel = <a className={`btn mb-1 ${country[1].collapsed ? `btn-link` : `btn-link`}`} onClick={() => { this.toggle(country[0]) }}>{country[0]}</a>;

                    return (
                      <TreeView key={`country-${country[0]}`} nodeLabel={countryLabel} defaultCollapsed={country[1].collapsed} collapsed={country[1].collapsed}>
                        {Object.entries(country[1].states).map((state) => {
                          const stateLabel = <a className={`btn mb-1 ${state[1].collapsed ? `btn-link` : `btn-link`}`} onClick={() => { this.toggle(country[0], state[0]) }}>{state[0]}</a>;

                          return (
                            <TreeView key={`state-${state[0]}`} nodeLabel={stateLabel} defaultCollapsed={state[1].collapsed} collapsed={state[1].collapsed}>
                              {Object.entries(state[1].sites).map((site) => {
                                return (
                                  <div className="btn-group mb-1 ml-4 d-block" key={`site-${site[0]}`}>
                                    <a className={`btn ${site[1].collapsed ? `btn-link` : `btn-primary text-white`}`} onClick={() => { this.setSiteSelection(site[0]) }}>{site[1].name}</a>
                                    <NavLink to={`/manage/sites/${site[0]}`} className={`btn ${site[1].collapsed ? `btn-link` : `btn-primary text-white`}`} title="Go to site"><i className="fas fa-arrow-circle-right" /></NavLink>
                                  </div>
                                );
                              })}
                            </TreeView>
                          )
                        })}
                      </TreeView>
                    )
                  })}

                </div>
              </CardBody>
              <div className=" mx-3" style={{ height: '30px' }}>
                <i className="cil-location-pin" style={{fontSize: '100px', right: '-10px', bottom: '-10px', position: 'absolute', opacity: 0.1, zIndex: 0}}></i>
              </div>
            </Card>

          </Col>

        </Row>

        <NewSite isOpen={this.state.newSiteModal} toggle={() => { this.setState({newSiteModal: !this.state.newSiteModal}) }} className={'m'} {...this.props} />

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    sites: state.firebase.data.sites,
    devices: state.firebase.data.devices,
    cameras: state.firebase.data.cameras,
    screens: state.firebase.data.screens
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSitesBySubscriptionID: SitesActions.getSitesBySubscriptionID,

    getDevicesBySubscriptionID: DevicesActions.getDevicesBySubscriptionID,
    getCamerasBySubscriptionID: CamerasActions.getCamerasBySubscriptionID,
    getScreensBySubscriptionID: ScreensActions.getScreensBySubscriptionID,

    getDevicesBySubscriptionIDAndBySiteID: DevicesActions.getDevicesBySubscriptionIDAndBySiteID,
    getCamerasBySubscriptionIDAndBySiteID: CamerasActions.getCamerasBySubscriptionIDAndBySiteID,
    getScreensBySubscriptionIDAndBySiteID: ScreensActions.getScreensBySubscriptionIDAndBySiteID,

    sortSitesByLocation: SitesActions.sortSitesByLocation

  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sites);
